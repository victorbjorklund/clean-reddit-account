#! /usr/bin/python3
import praw, time, sys, getpass

#Asks for the users login details
user = input("Reddit Username: ")
password = input("Reddit Password: ")

#The text that the script will replace your comments with before deleting them. The reason is that Reddit saves all deleted comments on their server but supposedly not the edits so this should make it more difficult to recover your comments. 
msg = '[Removed]'

# You might want to change the name of the script to something else to avoid that reddit thinks it's spam. It doesn't really matter what you name the script. 
r = praw.Reddit(user_agent='Clean Your Reddit Account')

# Logs into your reddit account
r.login(user, password, disable_warning=True)
user_object = r.get_redditor(user)

#Retrives the maximum amount of comments by the user (currently that is the 1000 latest comments)
comments = user_object.get_comments(limit = None)


x = 1

# If you have sent a integer as a command line argument to the program that number will be the amount of comments it doesn't delete. So 10 means it deletes all comments except the 10 most recent comments.
if (len(sys.argv) > 1) and sys.argv[1].isdigit():
    amountComments = int(sys.argv[1])

#if you haven't given any amount of comments to be saved it will delete all of them
else:
    amountComments = 0
# Loops through all your comments    
for comment in comments:
    if (x > amountComments ):
        #Edits the comment first with the "msg" string
        comment.edit(msg)
        #Deletes the comment
        comment.delete()
    x = x + 1

